#pragma once

class asIScriptEngine;
class asIScriptContext;
namespace AngelBinder{
	class Engine;
}
namespace CoreModule
{
	struct CORE_API iInterp
	{
		iInterp();
		AngelBinder::Engine* scriptEn;
		int RunScript(const char *scriptFile, bool debug);
		virtual void Initialize() = 0;
	protected:
		int _CompileScript(const char *scriptFile);
		int _ExecuteScript(const char *scriptFile, bool debug);
	};
	class CORE_API ScriptingSystem:public iInterp{
	public:
		ScriptingSystem();
		virtual void Initialize();
	protected:
		void RegisterCoreModuleMethods();
	private:
		void RegisterVector3();
		void RegisterRadian();
		void RegisterDegree();
		void RegisterQuaternion();
	private:
		// non copyable
		ScriptingSystem(const ScriptingSystem& other) = delete; // non construction-copyable
		ScriptingSystem& operator=(const ScriptingSystem&) = delete; // non copyable
	};
}