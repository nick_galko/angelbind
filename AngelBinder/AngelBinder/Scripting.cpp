/****************************************************************************
Autor:Nick Galko
nick.galko@vegaengine.com
nikgastovski@gmail.com
nikolay.galko@gmail.com

This is part is Vega Engine
License is BSD
****************************************************************************/

#include "CorePrivate.h"
#include "Scripting.h"
#include "..\inc\Script\angelscript.h"
#include "..\inc\Script\as_scriptengine.h"
#include "..\inc\Script\debugger.h"
#include "..\inc\Script\scriptbuilder.h"
#include "..\inc\Script\scriptstdstring.h"
#include "..\inc\Script\scriptarray.h"
#include "..\inc\Script\scriptfile.h"
#include "..\inc\Script\scripthelper.h"
#include "WrappedScriptFunctions.h"
#include "AngelBinder.h"

AB_TRANSLATE_TYPE(std::string, "string")
AB_TRANSLATE_TYPE(Ogre::Quaternion, "Quat")//Ogre::Quaternion
AB_TRANSLATE_TYPE(Ogre::Vector3, "Vec3")//Ogre::Vector3
AB_TRANSLATE_TYPE(Ogre::Radian, "Rad")//Ogre::Radian
AB_TRANSLATE_TYPE(Ogre::Degree, "Degree")//Ogre::Degree

namespace CoreModule
{
	/**
	*/
	int iInterp::RunScript(const char *scriptFile, bool debug){
		Debug("Loading script:%s", scriptFile);
		using namespace AngelBinder;
		try
		{
			Debug("Creation Module %s", scriptFile);
			Module* module = scriptEn->createModule(scriptFile);
			module->compile(scriptFile);
			_ExecuteScript(scriptFile, debug);
			return 1;
		}
		catch (std::exception& ex)
		{
			Warning("Compiling script FAILED. Reason: %s", std::string(ex.what()));
			return 0;
		}
		return 1;
	}

	void PrintScriptOutput(AngelBinder::Engine*, std::string msg)	{
		LogPrintf(msg.c_str());
	}
	/**
	*/
	iInterp::iInterp() :scriptEn(nullptr){}
	/**
	*/
	ScriptingSystem::ScriptingSystem() : iInterp(){
		// Create the script engine
		scriptEn = new AngelBinder::Engine(PrintScriptOutput);
		if (!scriptEn)
			Warning("[SCRIPT]Failed to create script engine.");
	}
	/**
	*/
	/**
	*/
	int iInterp::_ExecuteScript(const char *scriptFile, bool debug)
	{
		using namespace AngelBinder;
		CDebugger *dbg = 0;

		Module*mod = scriptEn->getModule(scriptFile);
		if (!mod) return -1;

		// Find the main function
		asIScriptFunction *func = mod->getFunctionByDecl("int main()");
		if (func == 0)
			func = mod->getFunctionByDecl("void main()");	// Try again with "void main()"


		if (func == 0)
		{
			Warning("[SCRIPT]Cannot find 'int main()' or 'void main()'");
			return -1;
		}
			asIScriptContext*ctx = scriptEn->_engine->CreateContext();
		if (debug)
		{
			std::cout << "[SCRIPT]Debugging, waiting for commands. Type 'h' for help." << std::endl;

			// Create the debugger instance
			dbg = new CDebugger();

			// Allow the user to initialize the debugging before moving on
			dbg->TakeCommands(ctx);
		}

		if (dbg)
		{
			// Set the line callback for the debugging
			ctx->SetLineCallback(asMETHOD(CDebugger, LineCallback), dbg, asCALL_THISCALL);
		}

		// Once we have the main function, we first need to initialize the global variables
		// Pass our own context so the initialization of the global variables can be debugged too
		int r = mod->getModule(scriptFile)->ResetGlobalVars(ctx);
		if (r < 0)
		{
			Warning("[SCRIPT]Failed while initializing global variables");
			return -1;
		}

		// Execute the script
		ctx->Prepare(func);
		r = ctx->Execute();
		if (r != asEXECUTION_FINISHED)
		{
			if (r == asEXECUTION_EXCEPTION)
			{
				Warning("[SCRIPT]The script '%s' failed with an exception:in '%s',line %i", scriptFile,
					ctx->GetExceptionString(), ctx->GetLineNumber());
				r = -1;
			}
			else if (r == asEXECUTION_ABORTED)
			{
				Warning("[SCRIPT]The script '%s' was aborted on %i",
					scriptFile, ctx->GetLineNumber());
				r = -1;
			}
			else
			{
				Warning("[SCRIPT]The script %s terminated unexpectedly", scriptFile);
				r = -1;
			}
		}
		else
		{
			// Get the return value from the script
			if (func->GetReturnTypeId() == asTYPEID_INT32)
			{
				r = *(int*)ctx->GetAddressOfReturnValue();
			}
			else
				r = 0;
		}
		ctx->Release();

		// Destroy debugger
		if (dbg)
			delete dbg;

		return r;
	}
	/**
	*/
	void ScriptingSystem::RegisterCoreModuleMethods()
	{
		RegisterStdString(scriptEn->asEngine());
		RegisterScriptArray(scriptEn->asEngine());
		RegisterStdStringUtils(scriptEn->asEngine());

		//Logging
		using namespace AngelBinder;
		Exporter::Export(*scriptEn)
			[
				Exporter::Functions()
				.def("LogPrintf", &ScriptLogPrintf)
				.def("Debug", &ScriptDebug)
				.def("SeriousWarning", &ScriptSeriousWarning)
				.def("Warning", &ScriptWarning)
			];

		RegisterRadian();
		RegisterDegree();
		RegisterVector3();
		RegisterQuaternion();
	}
	/**
	*/
	void ScriptingSystem::RegisterVector3(){
		using namespace Ogre;
		using namespace AngelBinder;

		/// Exports the structure
		Exporter::Export(*scriptEn)
			[
				Exporter::Class<Vector3>()
				.member("x", &Vector3::x)
				.member("y", &Vector3::y)
				.member("z", &Vector3::z)

				.ctor()
				.ctor<float, float, float>()
				.ctor<float>()
				.ctor<const Vector3&>()
				.dtor_dummy()
				.dtor()
				// Register the object methods
				.method("length", (Ogre::Real(__thiscall Ogre::Vector3::*)(const Ogre::Vector3 &))&Vector3::length)
				.method("squaredLength", (Ogre::Real(__thiscall Ogre::Vector3::*)(const Ogre::Vector3 &))&Vector3::squaredLength)
				.method("swap", (Ogre::Real(__thiscall Ogre::Vector3::*)(const Ogre::Vector3 &))&Vector3::swap)
				.method("distance", (Ogre::Real(__thiscall Ogre::Vector3::*)(const Ogre::Vector3 &))&Vector3::distance)
				.method("squaredDistance", (Ogre::Real(__thiscall Ogre::Vector3::*)(const Ogre::Vector3 &))&Vector3::squaredDistance)
				.method("dotProduct", (Ogre::Real(__thiscall Ogre::Vector3::*)(const Ogre::Vector3 &))&Vector3::dotProduct)
				.method("absDotProduct", (Ogre::Real(__thiscall Ogre::Vector3::*)(const Ogre::Vector3 &))&Vector3::absDotProduct)
				.method("normalise", (Ogre::Real(__thiscall Ogre::Vector3::*)(const Ogre::Vector3 &))&Vector3::normalise)
				.method("crossProduct", (Ogre::Vector3(__thiscall Ogre::Vector3::*)(const Ogre::Vector3 &))&Vector3::crossProduct)
				.method("midPoint", (Ogre::Vector3(__thiscall Ogre::Vector3::*)(const Ogre::Vector3 &))&Vector3::midPoint)
				.method("makeFloor", &Vector3::makeFloor)
				.method("makeCeil", &Vector3::makeCeil)
				.method("perpendicular", (Ogre::Vector3(__thiscall Ogre::Vector3::*)(const Ogre::Vector3 &))&Vector3::perpendicular)
				.method("randomDeviant", (Ogre::Vector3(__thiscall Ogre::Vector3::*)(const Ogre::Vector3 &))&Vector3::randomDeviant)
				.method("angleBetween", (Ogre::Vector3(__thiscall Ogre::Vector3::*)(const Ogre::Vector3 &))&Vector3::angleBetween)
				.method("getRotationTo", (Ogre::Vector3(__thiscall Ogre::Vector3::*)(const Ogre::Vector3 &))&Vector3::getRotationTo)
				.method("isZeroLength", (Ogre::Vector3(__thiscall Ogre::Vector3::*)(const Ogre::Vector3 &))&Vector3::isZeroLength)
				.method("normalisedCopy", (Ogre::Vector3(__thiscall Ogre::Vector3::*)(const Ogre::Vector3 &))&Vector3::normalisedCopy)
				.method("reflect", (Ogre::Vector3(__thiscall Ogre::Vector3::*)(const Ogre::Vector3 &))&Vector3::reflect)
				.method("positionEquals", (Ogre::Vector3(__thiscall Ogre::Vector3::*)(const Ogre::Vector3 &))&Vector3::positionEquals)
				.method("positionCloses", (Ogre::Vector3(__thiscall Ogre::Vector3::*)(const Ogre::Vector3 &))&Vector3::positionCloses)
				.method("directionEquals", (Ogre::Vector3(__thiscall Ogre::Vector3::*)(const Ogre::Vector3 &))&Vector3::directionEquals)
				.method("isNaN", (Ogre::Vector3(__thiscall Ogre::Vector3::*)(const Ogre::Vector3 &))&Vector3::isNaN)
				TODO("Clean operators after tests")
#if 0
				// Register the object operators
				r = sciptEn->RegisterObjectMethod("Vec3", "float opIndex(int) const", asMETHODPR(Vector3, operator[], (size_t) const, float), asCALL_THISCALL);
				r = sciptEn->RegisterObjectMethod("Vec3", "Vec3 &f(const Vec3 &in)", asMETHODPR(Vector3, operator =, (const Vector3 &), Vector3&), asCALL_THISCALL);
				r = sciptEn->RegisterObjectMethod("Vec3", "bool opEquals(const Vec3 &in) const", asMETHODPR(Vector3, operator==, (const Vector3&) const, bool), asCALL_THISCALL);

				r = sciptEn->RegisterObjectMethod("Vec3", "Vec3 opAdd(const Vec3 &in) const", asMETHODPR(Vector3, operator+, (const Vector3&) const, Vector3), asCALL_THISCALL);
				r = sciptEn->RegisterObjectMethod("Vec3", "Vec3 opSub(const Vec3 &in) const", asMETHODPR(Vector3, operator-, (const Vector3&) const, Vector3), asCALL_THISCALL);

				r = sciptEn->RegisterObjectMethod("Vec3", "Vec3 opMul(float) const", asMETHODPR(Vector3, operator*, (const float) const, Vector3), asCALL_THISCALL);
				r = sciptEn->RegisterObjectMethod("Vec3", "Vec3 opMul(const Vec3 &in) const", asMETHODPR(Vector3, operator*, (const Vector3&) const, Vector3), asCALL_THISCALL);
				r = sciptEn->RegisterObjectMethod("Vec3", "Vec3 opDiv(float) const", asMETHODPR(Vector3, operator/, (const float) const, Vector3), asCALL_THISCALL);
				r = sciptEn->RegisterObjectMethod("Vec3", "Vec3 opDiv(const Vec3 &in) const", asMETHODPR(Vector3, operator/, (const Vector3&) const, Vector3), asCALL_THISCALL);

				r = sciptEn->RegisterObjectMethod("Vec3", "Vec3 opAdd() const", asMETHODPR(Vector3, operator+, () const, const Vector3&), asCALL_THISCALL);
				r = sciptEn->RegisterObjectMethod("Vec3", "Vec3 opSub() const", asMETHODPR(Vector3, operator-, () const, Vector3), asCALL_THISCALL);

				//r = sciptEn->RegisterObjectMethod("Vec3", "Vec3 opMul(float, const Vec3 &in)", asMETHODPR(Vector3, operator*,(const float, const Vector3&), Vector3), asCALL_THISCALL); 

				r = sciptEn->RegisterObjectMethod("Vec3", "Vec3 &opAddAssign(const Vec3 &in)", asMETHODPR(Vector3, operator+=, (const Vector3 &), Vector3&), asCALL_THISCALL);
				r = sciptEn->RegisterObjectMethod("Vec3", "Vec3 &opAddAssign(float)", asMETHODPR(Vector3, operator+=, (const float), Vector3&), asCALL_THISCALL);

				r = sciptEn->RegisterObjectMethod("Vec3", "Vec3 &opSubAssign(const Vec3 &in)", asMETHODPR(Vector3, operator-=, (const Vector3 &), Vector3&), asCALL_THISCALL);
				r = sciptEn->RegisterObjectMethod("Vec3", "Vec3 &opSubAssign(float)", asMETHODPR(Vector3, operator-=, (const float), Vector3&), asCALL_THISCALL);

				r = sciptEn->RegisterObjectMethod("Vec3", "Vec3 &opMulAssign(const Vec3 &in)", asMETHODPR(Vector3, operator*=, (const Vector3 &), Vector3&), asCALL_THISCALL);
				r = sciptEn->RegisterObjectMethod("Vec3", "Vec3 &opMulAssign(float)", asMETHODPR(Vector3, operator*=, (const float), Vector3&), asCALL_THISCALL);

				//r = sciptEn->RegisterObjectMethod("Vec3", "Vec3& operator @= ( const Vec3& rkVector f( const Vector3& rkVector )", asMETHOD(Ogre::Vector3, f), asCALL_THISCALL); MYASSERT(r>=0);

				r = sciptEn->RegisterObjectMethod("Vec3", "Vec3 &opDivAssign(const Vec3 &in)", asMETHODPR(Vector3, operator/=, (const Vector3 &), Vector3&), asCALL_THISCALL);
				r = sciptEn->RegisterObjectMethod("Vec3", "Vec3 &opDivAssign(float)", asMETHODPR(Vector3, operator/=, (const float), Vector3&), asCALL_THISCALL);

				// r = sciptEn->RegisterObjectMethod("Vec3", "int opCmp(const Vec3 &in) const",        asFUNCTION(Vector3Cmp), asCALL_CDECL_OBJFIRST); 

#endif
			];
	}
	/**
	*/
	void ScriptingSystem::RegisterRadian()
	{
		using namespace Ogre;
		using namespace AngelBinder;
		Exporter::Export(*scriptEn)
			[
				Exporter::Class<Radian>()
				.ctor()
				.ctor<float>()
				.ctor<const Radian&>()
				.dtor_dummy()
				.dtor()
				.method("valueDegrees", (Ogre::Real(__thiscall Ogre::Radian::*)(void))&Radian::valueDegrees)
				.method("valueRadians", (Ogre::Real(__thiscall Ogre::Radian::*)(void))&Radian::valueRadians)
				.method("valueAngleUnits", (Ogre::Real(__thiscall Ogre::Radian::*)(void))&Radian::valueAngleUnits)
			];
		TODO("Operators if needed")
#if 0
			int r;
		using namespace Ogre;


		// Register other object behaviours
		r = sciptEn->RegisterObjectBehaviour("Rad", asBEHAVE_IMPLICIT_VALUE_CAST, "float f() const", asMETHOD(Radian,valueRadians), asCALL_THISCALL); 
		r = sciptEn->RegisterObjectBehaviour("Rad", asBEHAVE_IMPLICIT_VALUE_CAST, "double f() const", asMETHOD(Radian,valueRadians), asCALL_THISCALL); 

		// Register the object operators
		r = sciptEn->RegisterObjectMethod("Rad", "Rad &opAssign(const Rad &in)",      asMETHODPR(Radian, operator =, (const Radian &), Radian&), asCALL_THISCALL); 
		r = sciptEn->RegisterObjectMethod("Rad", "Rad &opAssign(const float)",       asMETHODPR(Radian, operator =, (const float &), Radian&), asCALL_THISCALL); 
		r = sciptEn->RegisterObjectMethod("Rad", "Rad &opAssign(const Degree &in)",      asMETHODPR(Radian, operator =, (const Degree &), Radian&), asCALL_THISCALL); 

		r = sciptEn->RegisterObjectMethod("Rad", "Rad opAdd() const",                    asMETHODPR(Radian, operator+,() const, const Radian&), asCALL_THISCALL); 
		r = sciptEn->RegisterObjectMethod("Rad", "Rad opAdd(const Rad &in) const",    asMETHODPR(Radian, operator+,(const Radian&) const, Radian), asCALL_THISCALL); 
		r = sciptEn->RegisterObjectMethod("Rad", "Rad opAdd(const Degree &in) const",    asMETHODPR(Radian, operator+,(const Degree&) const, Radian), asCALL_THISCALL); 

		r = sciptEn->RegisterObjectMethod("Rad", "Rad &opAddAssign(const Rad &in)",   asMETHODPR(Radian,operator+=,(const Radian &),Radian&), asCALL_THISCALL); 
		r = sciptEn->RegisterObjectMethod("Rad", "Rad &opAddAssign(const Degree &in)",   asMETHODPR(Radian,operator+=,(const Degree &),Radian&), asCALL_THISCALL); 

		r = sciptEn->RegisterObjectMethod("Rad", "Rad opSub() const",                    asMETHODPR(Radian, operator-,() const, Radian), asCALL_THISCALL); 
		r = sciptEn->RegisterObjectMethod("Rad", "Rad opSub(const Rad &in) const",    asMETHODPR(Radian, operator-,(const Radian&) const, Radian), asCALL_THISCALL); 
		r = sciptEn->RegisterObjectMethod("Rad", "Rad opSub(const Degree &in) const",    asMETHODPR(Radian, operator-,(const Degree&) const, Radian), asCALL_THISCALL); 

		r = sciptEn->RegisterObjectMethod("Rad", "Rad &opSubAssign(const Rad &in)",   asMETHODPR(Radian,operator-=,(const Radian &),Radian&), asCALL_THISCALL); 
		r = sciptEn->RegisterObjectMethod("Rad", "Rad &opSubAssign(const Degree &in)",   asMETHODPR(Radian,operator-=,(const Degree &),Radian&), asCALL_THISCALL); 

		r = sciptEn->RegisterObjectMethod("Rad", "Rad opMul(float) const",           asMETHODPR(Radian, operator*,(float) const, Radian), asCALL_THISCALL); 
		r = sciptEn->RegisterObjectMethod("Rad", "Rad opMul(const Rad &in) const",    asMETHODPR(Radian, operator*,(const Radian&) const, Radian), asCALL_THISCALL); 

		r = sciptEn->RegisterObjectMethod("Rad", "Rad &opMulAssign(float)",          asMETHODPR(Radian,operator*=,(float),Radian&), asCALL_THISCALL); 

		r = sciptEn->RegisterObjectMethod("Rad", "Rad opDiv(float) const",           asMETHODPR(Radian, operator/,(float) const, Radian), asCALL_THISCALL); 

		r = sciptEn->RegisterObjectMethod("Rad", "Rad &opDivAssign(float)",          asMETHODPR(Radian,operator*=,(float),Radian&), asCALL_THISCALL); 

		r = sciptEn->RegisterObjectMethod("Rad", "int opCmp(const Rad &in) const",       asFUNCTION(RadianCmp), asCALL_CDECL_OBJFIRST); 

		r = sciptEn->RegisterObjectMethod("Rad", "bool opEquals(const Rad &in) const",   asMETHODPR(Radian, operator==,(const Radian&) const, bool), asCALL_THISCALL); 

#endif
	}

	/**
	*/
	void ScriptingSystem::RegisterDegree()
	{
#if 0
		int r;
		using namespace Ogre;

		// Register other object behaviours
		r = sciptEn->RegisterObjectBehaviour("Degree", asBEHAVE_IMPLICIT_VALUE_CAST, "float f() const", asMETHOD(Degree,valueDegrees), asCALL_THISCALL); 
		r = sciptEn->RegisterObjectBehaviour("Degree", asBEHAVE_IMPLICIT_VALUE_CAST, "double f() const", asMETHOD(Degree,valueDegrees), asCALL_THISCALL); 

		// Register the object operators
		r = sciptEn->RegisterObjectMethod("Degree", "Degree &opAssign(const Degree &in)",      asMETHODPR(Degree, operator =, (const Degree &), Degree&), asCALL_THISCALL); 
		r = sciptEn->RegisterObjectMethod("Degree", "Degree &opAssign(float)",       asMETHODPR(Degree, operator =, (const float &), Degree&), asCALL_THISCALL); 
		r = sciptEn->RegisterObjectMethod("Degree", "Degree &opAssign(const Rad &in)",      asMETHODPR(Degree, operator =, (const Radian &), Degree&), asCALL_THISCALL); 

		r = sciptEn->RegisterObjectMethod("Degree", "Degree opAdd() const",                    asMETHODPR(Degree, operator+,() const, const Degree&), asCALL_THISCALL); 
		r = sciptEn->RegisterObjectMethod("Degree", "Degree opAdd(const Degree &in) const",    asMETHODPR(Degree, operator+,(const Degree&) const, Degree), asCALL_THISCALL); 
		r = sciptEn->RegisterObjectMethod("Degree", "Degree opAdd(const Rad &in) const",    asMETHODPR(Degree, operator+,(const Radian&) const, Degree), asCALL_THISCALL); 

		r = sciptEn->RegisterObjectMethod("Degree", "Degree &opAddAssign(const Degree &in)",   asMETHODPR(Degree,operator+=,(const Degree &),Degree&), asCALL_THISCALL); 
		r = sciptEn->RegisterObjectMethod("Degree", "Degree &opAddAssign(const Rad &in)",   asMETHODPR(Degree,operator+=,(const Radian &),Degree&), asCALL_THISCALL); 

		r = sciptEn->RegisterObjectMethod("Degree", "Degree opSub() const",                    asMETHODPR(Degree, operator-,() const, Degree), asCALL_THISCALL); 
		r = sciptEn->RegisterObjectMethod("Degree", "Degree opSub(const Degree &in) const",    asMETHODPR(Degree, operator-,(const Degree&) const, Degree), asCALL_THISCALL); 
		r = sciptEn->RegisterObjectMethod("Degree", "Degree opSub(const Rad &in) const",    asMETHODPR(Degree, operator-,(const Radian&) const, Degree), asCALL_THISCALL); 

		r = sciptEn->RegisterObjectMethod("Degree", "Degree &opSubAssign(const Degree &in)",   asMETHODPR(Degree,operator-=,(const Degree &),Degree&), asCALL_THISCALL); 
		r = sciptEn->RegisterObjectMethod("Degree", "Degree &opSubAssign(const Rad &in)",   asMETHODPR(Degree,operator-=,(const Radian &),Degree&), asCALL_THISCALL); 

		r = sciptEn->RegisterObjectMethod("Degree", "Degree opMul(float) const",           asMETHODPR(Degree, operator*,(float) const, Degree), asCALL_THISCALL); 
		r = sciptEn->RegisterObjectMethod("Degree", "Degree opMul(const Degree &in) const",    asMETHODPR(Degree, operator*,(const Degree&) const, Degree), asCALL_THISCALL); 

		r = sciptEn->RegisterObjectMethod("Degree", "Degree &opMulAssign(float)",          asMETHODPR(Degree,operator*=,(float),Degree&), asCALL_THISCALL); 

		r = sciptEn->RegisterObjectMethod("Degree", "Degree opDiv(float) const",           asMETHODPR(Degree, operator/,(float) const, Degree), asCALL_THISCALL); 

		r = sciptEn->RegisterObjectMethod("Degree", "Degree &opDivAssign(float)",          asMETHODPR(Degree,operator*=,(float),Degree&), asCALL_THISCALL); 

		r = sciptEn->RegisterObjectMethod("Degree", "int opCmp(const Degree &in) const",       asFUNCTION(DegreeCmp), asCALL_CDECL_OBJFIRST); 

		r = sciptEn->RegisterObjectMethod("Degree", "bool opEquals(const Degree &in) const",   asMETHODPR(Degree, operator==,(const Degree&) const, bool), asCALL_THISCALL); 

#else
		using namespace Ogre;
		using namespace AngelBinder;
		Exporter::Export(*scriptEn)
			[
				Exporter::Class<Degree>()
				.ctor()
				.ctor<float>()
				.ctor<const Degree&>()
				.dtor_dummy()
				.dtor()
				.method("valueRadians", (Ogre::Real(__thiscall Ogre::Degree::*)(void))&Degree::valueRadians)
				.method("valueDegrees", (Ogre::Real(__thiscall Ogre::Degree::*)(void))&Degree::valueDegrees)
				.method("valueAngleUnits", (Ogre::Real(__thiscall Ogre::Degree::*)(void))&Radian::valueAngleUnits)
			];
		TODO("Operators if needed")
#endif
	}

	/**
	*/
	void ScriptingSystem::RegisterQuaternion()
	{
		TODO("make this!")
#if 0
			int r;
		using namespace Ogre;

		// Register the object properties
		r = sciptEn->RegisterObjectProperty("Quat", "float w", offsetof(Quaternion, w)); 
		r = sciptEn->RegisterObjectProperty("Quat", "float x", offsetof(Quaternion, x)); 
		r = sciptEn->RegisterObjectProperty("Quat", "float y", offsetof(Quaternion, y)); 
		r = sciptEn->RegisterObjectProperty("Quat", "float z", offsetof(Quaternion, z)); 
		// r = sciptEn->RegisterObjectProperty("Quat", "float ms_fEpsilon", offsetof(Quaternion, ms_fEpsilon)); 
		// r = sciptEn->RegisterObjectProperty("Quat", "Quat ZERO", offsetof(Quaternion, ZERO)); 
		// r = sciptEn->RegisterObjectProperty("Quat", "Quat IDENTITY", offsetof(Quaternion, IDENTITY)); 


		// Register the object constructors
		r = sciptEn->RegisterObjectBehaviour("Quat", asBEHAVE_CONSTRUCT,  "void f()",                    asFUNCTION(QuaternionDefaultConstructor), asCALL_CDECL_OBJLAST); 
		r = sciptEn->RegisterObjectBehaviour("Quat", asBEHAVE_CONSTRUCT,  "void f(const Rad &in, const Vec3 &in)", asFUNCTION(QuaternionInitConstructor1), asCALL_CDECL_OBJLAST); 
		r = sciptEn->RegisterObjectBehaviour("Quat", asBEHAVE_CONSTRUCT,  "void f(float, float, float, float)", asFUNCTION(QuaternionInitConstructor2), asCALL_CDECL_OBJLAST); 
		r = sciptEn->RegisterObjectBehaviour("Quat", asBEHAVE_CONSTRUCT,  "void f(const Vec3 &in, const Vec3 &in, const Vec3 &in)", asFUNCTION(QuaternionInitConstructor3), asCALL_CDECL_OBJLAST); 
		r = sciptEn->RegisterObjectBehaviour("Quat", asBEHAVE_CONSTRUCT,  "void f(float)",           asFUNCTION(QuaternionInitConstructorScaler), asCALL_CDECL_OBJLAST); 
		r = sciptEn->RegisterObjectBehaviour("Quat", asBEHAVE_CONSTRUCT,  "void f(const Quat &in)",   asFUNCTION(QuaternionCopyConstructor), asCALL_CDECL_OBJLAST); 

		// Register the object operators
		r = sciptEn->RegisterObjectMethod("Quat", "float opIndex(int) const",                        asMETHODPR(Quaternion, operator[], (size_t) const, float), asCALL_THISCALL);
		r = sciptEn->RegisterObjectMethod("Quat", "Quat &opAssign(const Quat &in)",      asMETHODPR(Quaternion, operator =, (const Quaternion &), Quaternion&), asCALL_THISCALL); 
		r = sciptEn->RegisterObjectMethod("Quat", "Quat opAdd(const Quat &in) const",    asMETHODPR(Quaternion, operator+,(const Quaternion&) const, Quaternion), asCALL_THISCALL); 
		r = sciptEn->RegisterObjectMethod("Quat", "Quat opSub(const Quat &in) const",    asMETHODPR(Quaternion, operator-,(const Quaternion&) const, Quaternion), asCALL_THISCALL); 
		r = sciptEn->RegisterObjectMethod("Quat", "Quat opMul(const Quat &in) const",    asMETHODPR(Quaternion, operator*,(const Quaternion&) const, Quaternion), asCALL_THISCALL); 
		r = sciptEn->RegisterObjectMethod("Quat", "Quat opMul(float) const",               asMETHODPR(Quaternion, operator*,(float) const, Quaternion), asCALL_THISCALL); 
		r = sciptEn->RegisterObjectMethod("Quat", "Quat opSub() const",                        asMETHODPR(Quaternion, operator-,() const, Quaternion), asCALL_THISCALL); 
		r = sciptEn->RegisterObjectMethod("Quat", "bool opEquals(const Quat &in) const",       asMETHODPR(Quaternion, operator==,(const Quaternion&) const, bool), asCALL_THISCALL); 
		r = sciptEn->RegisterObjectMethod("Quat", "Vec3 opMul(const Vec3 &in) const",          asMETHODPR(Quaternion, operator*,(const Vector3&) const, Vector3), asCALL_THISCALL); 

		// Register the object methods
		r = sciptEn->RegisterObjectMethod("Quat", "float Dot(const Quat &in) const",    asMETHOD(Quaternion,Dot), asCALL_THISCALL); 
		r = sciptEn->RegisterObjectMethod("Quat", "float Norm() const",    asMETHOD(Quaternion,Norm), asCALL_THISCALL); 
		r = sciptEn->RegisterObjectMethod("Quat", "float normalise()",    asMETHOD(Quaternion,normalise), asCALL_THISCALL); 
		r = sciptEn->RegisterObjectMethod("Quat", "Quat Inverse() const",    asMETHOD(Quaternion,Inverse), asCALL_THISCALL); 
		r = sciptEn->RegisterObjectMethod("Quat", "Quat UnitInverse() const",    asMETHOD(Quaternion,UnitInverse), asCALL_THISCALL); 
		r = sciptEn->RegisterObjectMethod("Quat", "Quat Exp() const",    asMETHOD(Quaternion,Exp), asCALL_THISCALL); 
		r = sciptEn->RegisterObjectMethod("Quat", "Quat Log() const",    asMETHOD(Quaternion,Log), asCALL_THISCALL); 
		r = sciptEn->RegisterObjectMethod("Quat", "Rad getRoll(bool) const",    asMETHOD(Quaternion,getRoll), asCALL_THISCALL); 
		r = sciptEn->RegisterObjectMethod("Quat", "Rad getPitch(bool) const",    asMETHOD(Quaternion,getPitch), asCALL_THISCALL); 
		r = sciptEn->RegisterObjectMethod("Quat", "Rad getYaw(bool) const",    asMETHOD(Quaternion,getYaw), asCALL_THISCALL); 
		r = sciptEn->RegisterObjectMethod("Quat", "bool equals(const Quat &in, const Rad &in) const",    asMETHOD(Quaternion,equals), asCALL_THISCALL); 
		r = sciptEn->RegisterObjectMethod("Quat", "bool isNaN() const", asMETHOD(Quaternion,isNaN), asCALL_THISCALL); 

		// Register some static methods
		r = sciptEn->RegisterGlobalFunction("Quat Slerp(float, const Quat &in, const Quat &in, bool &in)",  asFUNCTIONPR(Quaternion::Slerp,(Real fT, const Quaternion&, const Quaternion&, bool), Quaternion), asCALL_CDECL); 
		r = sciptEn->RegisterGlobalFunction("Quat SlerpExtraSpins(float, const Quat &in, const Quat &in, int &in)",    asFUNCTION(Quaternion::SlerpExtraSpins), asCALL_CDECL); 
		r = sciptEn->RegisterGlobalFunction("void Intermediate(const Quat &in, const Quat &in, const Quat &in, const Quat &in, const Quat &in)",    asFUNCTION(Quaternion::Intermediate), asCALL_CDECL); 
		r = sciptEn->RegisterGlobalFunction("Quat Squad(float, const Quat &in, const Quat &in, const Quat &in, const Quat &in, bool &in)",    asFUNCTION(Quaternion::Squad), asCALL_CDECL); 
		r = sciptEn->RegisterGlobalFunction("Quat nlerp(float, const Quat &in, const Quat &in, bool &in)",    asFUNCTION(Quaternion::nlerp), asCALL_CDECL); 
#endif
	}

	/**
	*/
	void ScriptingSystem::Initialize(){
		RegisterCoreModuleMethods();
	}
}